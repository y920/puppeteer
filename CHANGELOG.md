### 初版

- タグ: v1.0.0

### websocket, bitmexラッパー対応

- タグ: v1.0.1

- websocketサンプル
```
- puppets/
  - sample1/
    - sample1.py
    - sample1.json
```
### 5秒ローソク足追加

- タグ: v1.0.2

### websocket再接続処理修正

- タグ: v1.0.3

### 資産状況通知対応

- タグ: v1.0.4
